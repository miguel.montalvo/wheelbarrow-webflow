(function klaviyoForms() {
  'use strict';

  const klaviyoForms = document.querySelectorAll('[wheelbarrow="klaviyo-form"] form');
  const klaviyoEndpoint = '//manage.kmail-lists.com/ajax/subscriptions/subscribe';

  klaviyoForms.forEach(function(form) {
    form.addEventListener('submit', handleFormSubmit);
  });

  function handleFormSubmit(event) {
    event.preventDefault();
    event.stopPropagation();

    const form = event.target;
    const formBlock = form.parentNode
    const successMessage = formBlock.querySelector('.w-form-done');
    const errorMessage = formBlock.querySelector('.w-form-fail');

    fetch(klaviyoEndpoint, {
      method: 'POST',
      body: new FormData(form)
    })
      .then(response => {
        if (!response.ok) {
          throw 'There was an error';
        }

        return response.json();
      })
      .then(response => handleResponse(response, form, successMessage, errorMessage))
      .catch(() => showErrorMessage(successMessage, errorMessage));
  }

  function handleResponse(response, form, successMessage, errorMessage) {
    const { errors } = response;

    if (errors.length) {
      throw 'There was an error';
    }

    hideForm(form);
    showSuccessMessage(successMessage, errorMessage);
  }

  function hideForm(form) {
    form.style.display = 'none';
  }

  function showSuccessMessage(successMessage, errorMessage) {
    successMessage.style.display = 'block';
    errorMessage.style.display = 'none';
  }

  function showErrorMessage(successMessage, errorMessage) {
    errorMessage.style.display = 'block';
    successMessage.style.display = 'none';
  }
}());
